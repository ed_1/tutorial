from flask import Flask, request, render_template, jsonify
import aiml
import os

app = Flask(__name__)

# Create our AIML kernel
aiml_kernel = aiml.Kernel()
# Load our .aiml files under the 'aiml' directory
aiml_kernel.learn(os.sep.join(['aiml', '*.aiml']))

@app.route('/', methods=['GET', 'POST'])
@app.route('/index')
def index():
    """
    Default route for serving our interface
    """
    return render_template('index.html')


@app.route('/message', methods=['GET', 'POST'])
def message():
    """
    Generates a JSON response to users message
    """
    # get users response
    message = request.form.get('message')

    # if users input is empty default to a greeting message
    if not message:
        message = 'Hello'

    # generate the response
    response = aiml_kernel.respond(message)

    # return the response as a JSON object
    return jsonify(message=response)


if __name__ == '__main__':
    app.run(host='localhost', debug=True)